#
# Cookbook Name:: bkw_ds_role
# Attributes:: default
#
# Copyright (C) 2019 Schuberg Philis
#
# Created by: Dick Tump <dtump@schubergphilis.com>
#

role_default['opendj']['replication']['hostSearch'] = nil
role_default['opendj']['user'] = 'opendj'
role_default['opendj']['group'] = 'opendj'
role_default['opendj']['home'] = '/opt/opendj'

role_default['opendj']['rootUserPassword'] = '4NnJLdVq9oxqUcVixWP4' # TODO: vault (cn=Directory Manager)
role_default['opendj']['adminPassword'] = '4NnJLdVq9oxqUcVixWP4' # Todo: vault (tbv replica admin)
role_default['opendj']['configDN'] = 'ou=am-config'
role_default['opendj']['backends']['suwiRoot']['baseDN'] = 'c=NL'
role_default['opendj']['backends']['suwiRoot']['organizations'] = %w(special suwi)
role_default['opendj']['backends']['userRoot']['baseDN'] = 'dc=bkwi,dc=nl'
role_default['opendj']['users']['special'] = %w(sa_reports sa_monitor sa_useradmin)
role_default['opendj']['ldapPort'] = '10389'
role_default['opendj']['ldapsPort'] = '10636'
role_default['opendj']['adminConnectorPort'] = '4444'
role_default['opendj']['replicationPort'] = '8989'
role_default['opendj']['hostname'] = node['fqdn']

role_default['opendj']['config-server-cert'] = 'config-server-cert'

role_default['opendj']['config']['global-configuration']['lookthrough-limit'] = '20000'
role_default['opendj']['config']['global-configuration']['smtp-server'] = '127.0.0.1:25'

role_default['opendj']['connection-handler']['LDAP']['enabled'] = 'false'
role_default['opendj']['connection-handler']['LDAPS']['allow-ldap-v2'] = 'true'

role_default['opendj']['log-publisher']['File-Based Access Logger']['enabled'] = 'false'

role_default['opendj']['password-validators']['Length-Based Password Validator'] = '--set enabled:true --set min-password-length:8 --set max-password-length:0'
role_default['opendj']['password-validators']['Similarity-Based Password Validator'] = '--set enabled:true --set min-password-difference:3'
role_default['opendj']['password-validators']['Attribute Value'] = '--set enabled:true --add match-attribute:cn --add match-attribute:sn --add match-attribute:givenName --add match-attribute:uid'
role_default['opendj']['password-validators']['Unique Characters'] = '--set enabled:true --set min-unique-characters:4 --set case-sensitive-validation:true'
role_default['opendj']['password-validators']['Character Set'] = '--set enabled:true --set allow-unclassified-characters:true'

role_default['opendj']['password-policies']['Default Password Policy']['config'] = '--set default-password-storage-scheme:"Salted SHA-512" --set deprecated-password-storage-scheme:"Salted SHA-1" --set allow-user-password-changes:true --set expire-passwords-without-warning:true --set password-expiration-warning-interval:"7 days" --set force-change-on-add:true --set force-change-on-reset:true --set "grace-login-count:2" --set "idle-lockout-interval:45 days" --set "last-login-time-attribute:ds-pwp-last-login-time" --set "last-login-time-format:yyyyMMddHHmmss" --set "lockout-duration:0 minutes" --set "lockout-failure-expiration-interval: 0 minutes" --set "lockout-failure-count:5" --set "max-password-age:56 days" --set "max-password-reset-age:14 days" --set "min-password-age:24 hours" --set "password-attribute:userPassword" --set password-change-requires-current-password:true --set "password-history-count:10" --set "password-history-duration:0 seconds" --set skip-validation-for-administrators:true --set require-secure-authentication:true --set require-secure-password-changes:true'
role_default['opendj']['password-policies']['Default Password Policy']['add-validators'] = ['Length-Based Password Validator', 'Similarity-Based Password Validator', 'Attribute Value', 'Unique Characters', 'Character Set']
role_default['opendj']['password-policies']['Default Password Policy']['remove-validators'] = ['At least 8 characters', 'Common passwords', 'Dictionary', 'Repeated Characters']

role_default['opendj']['password-policies']['Password Policy Service Accounts']['config'] = '--set default-password-storage-scheme:"Salted SHA-512" --set deprecated-password-storage-scheme:"Salted SHA-1" --set expire-passwords-without-warning:false --set password-expiration-warning-interval:"7 days" --set force-change-on-add:false --set force-change-on-reset:false --set "grace-login-count:10" --set "idle-lockout-interval:2000 days" --set "last-login-time-attribute:ds-pwp-last-login-time" --set "last-login-time-format:yyyyMMddHHmmss" --set "lockout-duration:1 s" --set "lockout-failure-expiration-interval:1 s" --set "lockout-failure-count:10000" --set "max-password-age:2000 days" --set "max-password-reset-age:8 hours" --set "min-password-age:24 hours" --set "password-attribute:userPassword" --set password-change-requires-current-password:true --set "password-history-count:5" --set "password-history-duration:0 seconds" --set skip-validation-for-administrators:true --set require-secure-authentication:true --set require-secure-password-changes:true'

role_default['opendj']['password-policies']['Password Policy JMX Service Accounts']['config'] = '--set default-password-storage-scheme:"Salted SHA-512" --set deprecated-password-storage-scheme:"Salted SHA-1" --set expire-passwords-without-warning:false --set password-expiration-warning-interval:"7 days" --set force-change-on-add:false --set force-change-on-reset:false --set "grace-login-count:10" --set "idle-lockout-interval:2000 days" --set "last-login-time-attribute:ds-pwp-last-login-time" --set "last-login-time-format:yyyyMMddHHmmss" --set "lockout-duration:480 s" --set "lockout-failure-expiration-interval:600 s" --set "lockout-failure-count:5" --set "max-password-age:2000 days" --set "max-password-reset-age:8 hours" --set "min-password-age:24 hours" --set "password-attribute:userPassword" --set password-change-requires-current-password:true --set "password-history-count:5" --set "password-history-duration:0 seconds" --set skip-validation-for-administrators:true --set require-secure-authentication:false --set require-secure-password-changes:true'
