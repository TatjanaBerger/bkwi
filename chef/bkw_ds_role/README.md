# bkw_ds_role

Cookbook to install ForgeRock DS

## Table of contents

1. [Requirements](#requirements)
    * [Platforms](#platforms)
    * [Cookbooks](#cookbooks)
2. [Usage](#usage)
3. [Attributes](#attributes)
4. [Recipes](#recipes)
    * [Public Recipes](#public-recipes)
5. [Versioning](#versioning)
6. [Testing](#testing)
7. [License and Author](#license-and-author)
8. [Contributing](#contributing)

## Requirements

### Platforms

This cookbook supports:

### Cookbooks

This cookbook does not depend on any other cookbooks.

## Usage

TODO: *Explain how to use the cookbook*

## Attributes

Attributes in this cookbook:

<table>
  <tr>
    <th>Key</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
  </tr>
</table>

## Recipes

### Public Recipes

#### `bkw_ds_role::default`

Customer role cookbook

## Versioning

This cookbook uses [Semantic Versioning 2.0.0](http://semver.org/).

Given a version number MAJOR.MINOR.PATCH, increment the:

* MAJOR version when you make functional cookbook changes,
* MINOR version when you add functionality in a backwards-compatible manner,
* PATCH version when you make backwards-compatible bug fixes.

## Testing

    rake foodcritic                     # Run Foodcritic lint checks
    rake integration                    # Alias for kitchen:all
    rake kitchen:all                    # Run all test instances
    rake kitchen:default-centos-7       # Run default-centos-7 test instance
    rake kitchen:default-windows2012r2  # Run default-windows2012r2 test instance
    rake rubocop                        # Run RuboCop style and lint checks
    rake rubocop:auto_correct           # Auto-correct RuboCop offenses
    rake spec                           # Run ChefSpec examples
    rake test                           # Run all tests

## License and Author

Authors and contributors:

Copyright (c) 2016, Schuberg Philis, All Rights Reserved.

## Contributing

Create a feature branch, commit your changes and push your new branch, then open a pull request for a colleague to review:

1. Create your feature branch (`git checkout -b my-new-feature`)
2. Commit your changes (`git commit -am 'Add some feature'`)
3. Push to the branch (`git push origin my-new-feature`)
4. Create a new pull request
