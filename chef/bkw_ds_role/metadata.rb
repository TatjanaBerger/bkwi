# ~FC078 Disable  : Ensure cookbook shared under an OSI-approved open source license
name             'bkw_ds_role'
maintainer       'BKWI'
maintainer_email 'int-bkwi@schubergphilis.com'
license          'All rights reserved'
description      'Cookbook to install ForgeRock DS'
version          '0.3.6'
source_url       'https://sbp.gitlab.schubergphilis.com/BKW/bkw_ds_role'
issues_url       'https://sbp.gitlab.schubergphilis.com/BKW/bkw_ds_role/issues'

chef_version     '>= 12.5'
supports 'centos'

depends 'java', '= 3.2.1'
depends 'bkw_java_wrapper', '= 0.1.4'
depends 'limits', '= 1.0.0'
