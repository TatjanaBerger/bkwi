#
# Cookbook:: bkw_ds_role
# Recipe:: configure_ds
#
# Copyright:: 2020, The Authors, All Rights Reserved.

node['opendj']['config'].each do |component, configarray|
  configarray.each do |setting, value|
    execute "set #{component} setting #{setting}" do
      command "#{node['opendj']['home']}/bin/dsconfig set-#{component}-prop --hostname #{node['opendj']['hostname']} --port #{node['opendj']['adminConnectorPort']} --bindDN 'cn=Directory Manager' -j /root/.opendjpassword --set '#{setting}:#{value}' --no-prompt --trustAll"
      user node['opendj']['user']
      not_if { shell_out("#{node['opendj']['home']}/bin/dsconfig get-#{component}-prop --hostname #{node['opendj']['hostname']} --port #{node['opendj']['adminConnectorPort']} --bindDN 'cn=Directory Manager' -j /root/.opendjpassword --property '#{setting}' --no-prompt --trustAll -s").stdout.chomp.match?(/\s#{value}$/) }
    end
  end
end

node['opendj']['connection-handler'].each do |handler, configarray|
  configarray.each do |setting, value|
    execute "set connection-handler #{handler} setting #{setting}" do
      command "#{node['opendj']['home']}/bin/dsconfig set-connection-handler-prop --handler-name #{handler} --hostname #{node['opendj']['hostname']} --port #{node['opendj']['adminConnectorPort']} --bindDN 'cn=Directory Manager' -j /root/.opendjpassword --set '#{setting}:#{value}' --no-prompt --trustAll"
      user node['opendj']['user']
      not_if { shell_out("#{node['opendj']['home']}/bin/dsconfig get-connection-handler-prop --handler-name #{handler} --hostname #{node['opendj']['hostname']} --port #{node['opendj']['adminConnectorPort']} --bindDN 'cn=Directory Manager' -j /root/.opendjpassword --property '#{setting}' --no-prompt --trustAll -s").stdout.chomp.match?(/\s#{value}$/) }
    end
  end
end

node['opendj']['log-publisher'].each do |logger, configarray|
  configarray.each do |setting, value|
    execute "set log-publisher #{logger} setting #{setting}" do
      command "#{node['opendj']['home']}/bin/dsconfig set-log-publisher-prop --publisher-name '#{logger}' --hostname #{node['opendj']['hostname']} --port #{node['opendj']['adminConnectorPort']} --bindDN 'cn=Directory Manager' -j /root/.opendjpassword --set '#{setting}:#{value}' --no-prompt --trustAll"
      user node['opendj']['user']
      not_if { shell_out("#{node['opendj']['home']}/bin/dsconfig get-log-publisher-prop --publisher-name '#{logger}' --hostname #{node['opendj']['hostname']} --port #{node['opendj']['adminConnectorPort']} --bindDN 'cn=Directory Manager' -j /root/.opendjpassword --property '#{setting}' --no-prompt --trustAll -s").stdout.chomp.match?(/\s#{value}$/) }
    end
  end
end

cookbook_file "#{node['opendj']['home']}/db/schema/suwinetPerson.ldif" do
  source 'suwinetPerson.ldif'
  mode '0640'
  owner node['opendj']['user']
  group node['opendj']['group']
end

node['opendj']['password-validators'].each do |validator, config|
  execute "configure password validator #{validator}" do
    command "./dsconfig set-password-validator-prop -p 10636 -D 'cn=Directory Manager' -j /root/.opendjpassword -X --no-prompt --validator-name '#{validator}' #{config}"
    cwd "#{node['opendj']['home']}/bin"
    user node['opendj']['user']
    action :run
  end
end

node['opendj']['password-policies'].each do |policyname, policy|
  dsconfig_command = if File.exist?("#{node['opendj']['home']}/bin/dsconfig") && shell_out("#{node['opendj']['home']}/bin/dsconfig get-password-policy-prop -p 10636 -D 'cn=Directory Manager' -j /root/.opendjpassword -X --no-prompt --policy-name '#{policyname}'").stderr.include?('Password Policy does not exist')
                       './dsconfig create-password-policy -p 10636 -D "cn=Directory Manager" -j /root/.opendjpassword --type password-policy -X --no-prompt'
                     else
                       './dsconfig set-password-policy-prop -p 10636 -D "cn=Directory Manager" -j /root/.opendjpassword -X --no-prompt'
                     end

  execute "create/configure password policy #{policyname}" do
    command "#{dsconfig_command} --policy-name '#{policyname}' #{policy['config']}"
    cwd "#{node['opendj']['home']}/bin"
    user node['opendj']['user']
    action :run
  end

  if policy['add-validators']
    policy['add-validators'].each do |validator|
      execute "Add validator #{validator} to #{policyname}" do
        command "./dsconfig set-password-policy-prop -p 10636 -D 'cn=Directory Manager' -j /root/.opendjpassword -X --no-prompt --policy-name '#{policyname}' --add 'password-validator:#{validator}'"
        cwd "#{node['opendj']['home']}/bin"
        user node['opendj']['user']
        action :run
      end
    end
  end

  next unless policy['remove-validators']
  policy['remove-validators'].each do |validator|
    execute "Remove validator #{validator} from #{policyname}" do
      command "./dsconfig set-password-policy-prop -p 10636 -D 'cn=Directory Manager' -j /root/.opendjpassword -X --no-prompt --policy-name '#{policyname}' --remove 'password-validator:#{validator}'"
      cwd "#{node['opendj']['home']}/bin"
      user node['opendj']['user']
      only_if "./dsconfig get-password-policy-prop -p 10636 -D 'cn=Directory Manager' -j /root/.opendjpassword -X --no-prompt --policy-name '#{policyname}' --property 'password-validator' -s | grep -q '#{validator}'"
      action :run
    end
  end
end
