#
# Cookbook:: bkw_ds_role
# Recipe:: install_ds
#
# Copyright:: 2019, The Authors, All Rights Reserved.

# Installing the DS as configuration store
execute 'Installing the DS as configuration store' do
  command "./setup directory-server --instancePath #{node['opendj']['home']} \
--rootUserDn cn='Directory Manager' \
--rootUserPassword #{node['opendj']['rootUserPassword']} \
--hostname #{node['opendj']['hostname']} \
--adminConnectorPort #{node['opendj']['adminConnectorPort']} \
--ldapPort #{node['opendj']['ldapPort']} \
--enableStartTls \
--productionMode \
--ldapsPort #{node['opendj']['ldapsPort']} \
--profile am-config:6.5 \
--set am-config/amConfigAdminPassword:#{node['opendj']['rootUserPassword']} \
--acceptLicense \
--doNotStart"
  cwd node['opendj']['home']
  sensitive true
  action :run
  notifies :run, 'execute[fix DS permissions after install]', :immediately
  notifies :run, 'execute[install opendj init script]', :immediately
end

execute 'fix DS permissions after install' do
  command "chown -R #{node['opendj']['user']}:#{node['opendj']['group']} #{node['opendj']['home']}"
  action :nothing
end

execute 'install opendj init script' do
  command "#{node['opendj']['home']}/bin/create-rc-script --outputFile /etc/init.d/opendj -u #{node['opendj']['user']} && /usr/bin/systemctl daemon-reload"
  action :nothing
end

# make sure the service is running
service 'opendj' do
  action [ :enable, :start ]
end
