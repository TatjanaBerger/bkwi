load_vault_secret "node.default['opendj']['rootUserPassword']" do
  vault_name "openam_#{node.chef_environment}"
  vault_item 'credentials'
  item_part 'item["Directory Manager"]'
end

load_vault_secret "node.default['opendj']['replication']['adminPassword']" do
  vault_name "openam_#{node.chef_environment}"
  vault_item 'credentials'
  item_part 'item["Directory Manager"]'
end

load_vault_secret "node.default['opendj']['backupUserPassword']" do
  vault_name "openam_#{node.chef_environment}"
  vault_item 'credentials'
  item_part 'item["Directory Manager"]'
end

load_vault_secret "node.default['opendj']['sa_reports']" do
  vault_name "openam_#{node.chef_environment}"
  vault_item 'credentials'
  item_part 'item["sa_reports"]'
end

load_vault_secret "node.default['opendj']['sa_monitor']" do
  vault_name "openam_#{node.chef_environment}"
  vault_item 'credentials'
  item_part 'item["sa_monitor"]'
end

load_vault_secret "node.default['opendj']['sa_useradmin']" do
  vault_name "openam_#{node.chef_environment}"
  vault_item 'credentials'
  item_part 'item["sa_useradmin"]'
end

load_vault_secret "node.default['opendj']['sa_openamadm']" do
  vault_name "openam_#{node.chef_environment}"
  vault_item 'credentials'
  item_part 'item["sa_openamadm"]'
end
