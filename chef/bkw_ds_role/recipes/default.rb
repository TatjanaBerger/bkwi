#
# Cookbook:: bkw_ds_role
# Recipe:: default
#
# Copyright:: 2019, The Authors, All Rights Reserved.

include_recipe 'bkw_java_wrapper'

include_recipe "#{cookbook_name}::vault"

group node['opendj']['group'] do
  action :create
end

user node['opendj']['user'] do
  group node['opendj']['group']
  home node['opendj']['home']
  action :create
end

set_limit node['opendj']['user'] do
  type 'soft'
  item 'nofile'
  value 65_536
end

set_limit node['opendj']['user'] do
  type 'hard'
  item 'nofile'
  value 131_072
end

package 'opendj'

# when the response of the DS status contains "The server has not been configured" we need to setup DS
ruby_block 'Install DS' do
  block do
    run_context.include_recipe "#{cookbook_name}::install_ds"
    run_context.include_recipe "#{cookbook_name}::set_certificate"
  end
  only_if { File.exist?("#{node['opendj']['home']}/bin/status") && shell_out("#{node['opendj']['home']}/bin/status --offline").stderr.include?('The server has not been configured') }
end

directory "#{node['opendj']['home']}/bkwi" do
  user node['opendj']['user']
  group node['opendj']['group']
end

include_recipe "#{cookbook_name}::configure_ds"

include_recipe "#{cookbook_name}::configure_userstore"

# search for hosts with same cookbook
node.normal['opendj']['replication']['hostSearch'] = "recipes:#{cookbook_name} AND chef_environment:#{node.chef_environment}"
replication_nodes = search(:node, node['opendj']['replication']['hostSearch'])

# find the replication_node
replication_nodes.each do |n|
  if n['fqdn'] == node['fqdn']
    node.normal['opendj']['replication']['source_node'] = n['fqdn']
  else
    node.normal['opendj']['replication']['replication_node'] = n['fqdn']
  end
end

if node['opendj']['replication'].key?('source_node') && node['opendj']['replication'].key?('replication_node')
  log '2+ DS instances found, replication enabled'
  include_recipe "#{cookbook_name}::configure_replication"
else
  log 'Single DS instance, replication disabled'
end

include_recipe "#{cookbook_name}::configure_users"
