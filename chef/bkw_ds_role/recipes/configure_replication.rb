#
# Cookbook:: bkw_ds_role
# Recipe:: configure_replication
#
# Copyright:: 2019, The Authors, All Rights Reserved.

dnlist = []
dnlist.push node['opendj']['configDN'] if node['opendj'].key?('configDN')
node['opendj']['backends'].each do |backend|
  dnlist.push backend
end

dnlist.each do |dn|
  execute "configure replication #{dn}" do
    command "./dsreplication configure \
--host1 #{node['opendj']['replication']['source_node']} \
--port1 #{node['opendj']['adminConnectorPort']} \
--bindDn1 cn='Directory Manager' \
--bindPassword1 #{node['opendj']['rootUserPassword']} \
--secureReplication1 \
--host2 #{node['opendj']['replication']['replication_node']} \
--port2 #{node['opendj']['adminConnectorPort']} \
--bindDn2 cn='Directory Manager' \
--bindPassword2 #{node['opendj']['rootUserPassword']} \
--secureReplication2 \
--replicationPort1 #{node['opendj']['replicationPort']} \
--replicationPort2 #{node['opendj']['replicationPort']} \
--baseDN #{dn} \
--adminUid admin \
--adminPassword #{node['opendj']['adminPassword']} \
--no-prompt \
--trustAll"
    cwd "#{node['opendj']['home']}/bin"
    user node['opendj']['user']
    sensitive true
    # Do not configure replication if dsreplication returns 'true' for 'Replication enabled' (4th field)
    not_if { shell_out("#{node['opendj']['home']}/bin/dsreplication status --port #{node['opendj']['adminConnectorPort']} --hostname #{node['opendj']['replication']['source_node']} --adminUid admin --adminPassword #{node['opendj']['adminPassword']} --trustAll -s --no-prompt -b #{dn} | awk '{ print $4 }'").stdout.include?('true') }
    # Do not configure replication if dsreplication returns 'Invalid Credentials' and replication log is not empty (this is to check if it's a fresh install)
    not_if { shell_out("#{node['opendj']['home']}/bin/dsreplication status --port #{node['opendj']['adminConnectorPort']} --hostname #{node['opendj']['replication']['source_node']} --adminUid admin --adminPassword #{node['opendj']['adminPassword']} --trustAll -s --no-prompt -b #{dn}").stderr.include?('Invalid Credentials') && !File.zero?("#{node['opendj']['home']}/logs/replication") }
    action :run
    notifies :run, "execute[initialize replication #{dn}]", :immediately
  end

  execute "initialize replication #{dn}" do
    command "./dsreplication initialize-all \
  --hostname #{node['opendj']['replication']['source_node']} \
  --port #{node['opendj']['adminConnectorPort']} \
  --baseDn #{dn} \
  --adminUid admin \
  --adminPassword #{node['opendj']['adminPassword']} \
  --no-prompt \
  --trustAll"
    cwd "#{node['opendj']['home']}/bin"
    user node['opendj']['user']
    sensitive true
    action :nothing
  end
end
