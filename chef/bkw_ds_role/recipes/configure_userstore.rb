#
# Cookbook:: bkw_ds_role
# Recipe:: configure_userstore
#
# Copyright:: 2019, The Authors, All Rights Reserved.

node['opendj']['backends'].each do |backend, properties|
  template "#{node['opendj']['home']}/bkwi/userstore.#{backend}.ldif" do
    source "userstore.#{backend}.ldif.erb"
    variables(baseDN: properties['baseDN'])
    user node['opendj']['user']
    group node['opendj']['group']
  end

  script "Create backend #{backend}" do
    interpreter 'bash'
    code <<-EOH
      #{node['opendj']['home']}/bin/dsconfig create-backend --hostname #{node['opendj']['hostname']} --port #{node['opendj']['adminConnectorPort']} --bindDN "cn=Directory Manager" --bindPassword #{node['opendj']['adminPassword']} --set base-dn:#{properties['baseDN']} --set enabled:true --set db-cache-percent:5 --type je --backend-name #{backend} --no-prompt --trustAll
      #{node['opendj']['home']}/bin/ldapmodify --hostName #{node['opendj']['hostname']} --port #{node['opendj']['adminConnectorPort']} --bindDN "cn=Directory Manager" --bindPassword #{node['opendj']['adminPassword']} --trustAll --fileName #{node['opendj']['home']}/bkwi/userstore.#{backend}.ldif --useSSL
      EOH
    sensitive true
    user node['opendj']['user']
    only_if { File.exist?("#{node['opendj']['home']}/bin/ldapsearch") && shell_out("#{node['opendj']['home']}/bin/ldapsearch --hostname #{node['opendj']['hostname']} --port #{node['opendj']['adminConnectorPort']} --bindDN \"cn=Directory Manager\" --bindPassword #{node['opendj']['adminPassword']} --baseDn #{properties['baseDN']} --useSSL --trustAll \"objectclass=top\"").stdout.include?('No Such Entry') }
  end

  next unless properties['organizations']
  properties['organizations'].each do |org|
    template "#{node['opendj']['home']}/bkwi/organization.#{org}.ldif" do
      source 'organization.ldif.erb'
      variables(organization: org, baseDN: properties['baseDN'])
      user node['opendj']['user']
      group node['opendj']['group']
    end

    script "Create org #{org} in backend #{backend}" do
      interpreter 'bash'
      code <<-EOH
          #{node['opendj']['home']}/bin/ldapmodify --hostName #{node['opendj']['hostname']} --port #{node['opendj']['adminConnectorPort']} --bindDN "cn=Directory Manager" --bindPassword #{node['opendj']['adminPassword']} --trustAll --fileName #{node['opendj']['home']}/bkwi/organization.#{org}.ldif --useSSL
        EOH
      sensitive true
      user node['opendj']['user']
      only_if { File.exist?("#{node['opendj']['home']}/bin/ldapsearch") && shell_out("#{node['opendj']['home']}/bin/ldapsearch --hostname #{node['opendj']['hostname']} --port #{node['opendj']['adminConnectorPort']} --bindDN \"cn=Directory Manager\" --bindPassword #{node['opendj']['adminPassword']} --baseDn o=#{org},#{properties['baseDN']} --useSSL --trustAll \"objectclass=organization\"").stdout.include?('No Such Entry') }
    end
  end
end
