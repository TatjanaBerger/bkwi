#
# Cookbook:: bkw_ds_role
# Recipe:: configure_userstore
#
# Copyright:: 2019, The Authors, All Rights Reserved.

node['opendj']['users']['special'].each do |user|
  filename = "special.#{user}.ldif"

  cookbook_file "#{node['opendj']['home']}/bkwi/#{filename}" do
    source filename
    mode '0640'
    owner node['opendj']['user']
    group node['opendj']['group']
  end

  script "Create user #{user}" do
    interpreter 'bash'
    code <<-EOH
      #{node['opendj']['home']}/bin/ldapmodify --hostName #{node['opendj']['hostname']} --port #{node['opendj']['adminConnectorPort']} --bindDN "cn=Directory Manager" --bindPassword #{node['opendj']['adminPassword']} --trustAll --fileName #{node['opendj']['home']}/bkwi/#{filename} --useSSL
      #{node['opendj']['home']}/bin/ldappasswordmodify --hostName #{node['opendj']['hostname']} --port #{node['opendj']['adminConnectorPort']} --bindDN "cn=Directory Manager" --bindPassword #{node['opendj']['adminPassword']} --trustAll --useSSL -a "cn=#{user},o=special,c=NL" -n '#{node['opendj'][user]}'
      EOH
    sensitive true
    user node['opendj']['user']
    only_if { File.exist?("#{node['opendj']['home']}/bin/ldapsearch") && shell_out("#{node['opendj']['home']}/bin/ldapsearch --hostname #{node['opendj']['hostname']} --port #{node['opendj']['adminConnectorPort']} --bindDN \"cn=Directory Manager\" --bindPassword #{node['opendj']['adminPassword']} --baseDn cn=#{user},o=special,c=NL --useSSL --trustAll \"objectclass=person\"").stdout.include?('No Such Entry') }
  end
end

template "#{node['opendj']['home']}/bkwi/passwordpolicy.am-config.ldif" do
  source 'passwordpolicy.am-config.ldif.erb'
  user node['opendj']['user']
  group node['opendj']['group']
end

execute 'set correct password policy for am-config' do
  command "./ldapmodify --hostName #{node['opendj']['hostname']} --port #{node['opendj']['adminConnectorPort']} --bindDN 'cn=Directory Manager' --bindPassword #{node['opendj']['adminPassword']} --trustAll --fileName #{node['opendj']['home']}/bkwi/passwordpolicy.am-config.ldif --useSSL"
  cwd "#{node['opendj']['home']}/bin"
  user node['opendj']['user']
  only_if "./ldapsearch -p #{node['opendj']['adminConnectorPort']} --bindDN 'cn=Directory Manager' -j /root/.opendjpassword --baseDn 'uid=am-config,ou=admins,#{node['opendj']['configDN']}' --useSSL --trustAll 'objectClass=top'"
  action :run
end
