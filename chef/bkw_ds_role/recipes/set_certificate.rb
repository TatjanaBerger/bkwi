#
# Cookbook:: bkw_ds_role
# Recipe:: set_certificate
#
# Copyright:: 2019, The Authors, All Rights Reserved.

# set certificate
execute 'Installing the DS as configuration store' do
  command "./dsconfig set-connection-handler-prop \
--handler-name LDAPS \
--add ssl-cert-nickname:#{node['opendj']['config-server-cert']} \
--hostname #{node['opendj']['hostname']} \
--port #{node['opendj']['adminConnectorPort']} \
--bindDn 'cn=Directory Manager' \
--bindPassword #{node['opendj']['adminPassword']} \
--trustAll \
--no-prompt"
  cwd "#{node['opendj']['home']}/bin"
  sensitive true
  user node['opendj']['user']
  action :run
end
