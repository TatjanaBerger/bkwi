require 'spec_helper'

# Spec test somehow fails when more than one environment is specified
ENVIRONMENTS = %w( production ).freeze
# ENVIRONMENTS = %w( production acceptance test demo ).freeze

describe 'bkw_ds_role::default' do
  before(:each) do
    allow_any_instance_of(Chef::Recipe).to receive(:search).and_return([{ 'fqdn' => 'bkwtapp19.bkwt.local' }])
    allow_any_instance_of(Chef::Recipe).to receive(:load_vault_secret).and_return(true)
    allow_any_instance_of(Chef::Recipe).to receive(:include_recipe).and_call_original
  end

  ENVIRONMENTS.each do |env_name|
    context "Centos 7.6.1810 #{env_name}" do
      let(:chef_run) do
        ChefSpec::ServerRunner.new(
          platform: 'centos',
          version: '7.6.1810') do |node, server|
          server.create_environment(env_name, default_attributes: { description: '...' })
          node.chef_environment = env_name
          node.automatic_attrs['hostname'] = 'bkwtapp19'
          node.automatic_attrs['x509']['cacert'] = 'fakecacert'
          node.automatic_attrs['bkwi']['algemeen']['jmx_password']['monitorRole'] = 'fakepassword'
          node.automatic_attrs['x509']['ca'] = 'fakeca'
          node.name 'bkwtapp19'
        end.converge(described_recipe)
      end
      let(:node) { chef_run.node }

      it 'converges successfully' do
        expect { chef_run }.to_not raise_error
      end
    end
  end
end
